<?php

/**
 * @file
 * Autocomplete suggestions.
 */

// Source bitmaps.
const SUGGESTION_CONTENT = 1;
const SUGGESTION_PRIORITY = 2;
const SUGGESTION_SURFER = 4;

/**
 * AJAX search autocomplete callback.
 *
 * @param string $txt
 *   The search string.
 *
 * @return string
 *   A JSON array of search suggestions.
 */
function suggestion_autocomplete($txt = '') {
  $txt = preg_replace(array('/^[^a-z]+/', '/[^a-z]+$/'), array('', ' '), strtolower($txt));

  if (strlen($txt) < variable_get('suggestion_min', 4)) {
    return drupal_json_output(array('' => ''));
  }
  $limit = variable_get('suggestion_limit', 20);

  $stmt = "
    SELECT
      ngram,
      ngram
    FROM
      {suggestion}
    WHERE
      ngram LIKE :ngram
      AND atoms <= :atoms
    ORDER BY
      qty DESC,
      ngram ASC,
      atoms ASC
  ";
  $args = array(
    ':ngram' => db_like($txt) . '%',
    ':atoms' => str_word_count($txt) + 2,
  );
  $suggestions = db_query_range($stmt, 0, $limit, $args)->fetchAllKeyed();

  if (count($suggestions) < $limit) {
    $args[':ngram'] = '%' . $args[':ngram'];
    $suggestions += db_query_range($stmt, 0, ($limit - count($suggestions)), $args)->fetchAllKeyed();
  }
  return count($suggestions)? drupal_json_output($suggestions): drupal_json_output(array('' => ''));
}
/**
 * Implements hook_block_info().
 */
function suggestion_block_info () {
  return array(
    'suggestion-index' => array(
      'info'       => t('Suggestion Indexing'),
      'cache'      => DRUPAL_NO_CACHE,
      'pages'      => 'admin/config/suggestion',
      'region'     => 'content',
      'status'     => 1,
      'visibility' => BLOCK_VISIBILITY_LISTED,
      'weight'     => -100,
    ),
  );
}
/**
 * Implements hook_block_view().
 */
function suggestion_block_view ($delta = '') {

  switch ($delta) {
    case 'suggestion-index':
      return array(
        'title'   => t('Suggestion Indexing'),
        'content' => drupal_render(drupal_get_form('suggestion_index_block_form')),
      );
  }
  return array();
}
/**
 * Implementation of hook_cron().
 */
function suggestion_cron() {
  if (!variable_get('suggestion_synced', TRUE)) {
    suggestion_index();
  }
}
/**
 * Implements hook_form_alter().
 */
function suggestion_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == variable_get('suggestion_form_id', '') && !empty($form[variable_get('suggestion_field_name', '')])) {
    $form[variable_get('suggestion_field_name', '')]['#autocomplete_path'] = 'suggestion/autocomplete';
    $form['#submit'][] = 'suggestion_surfer_submit';
  }
}
/**
 * Build the suggestion index.
 *
 * @param int $last_nid
 *   The Node ID of the last node batched.
 * @param int $limit
 *   The number of rows to process.
 */
function suggestion_index($last_nid = 0, $limit = PHP_INT_MAX) {
  $count = &drupal_static(__FUNCTION__ . '_count', 0);
  $nid = &drupal_static(__FUNCTION__ . '_nid', 0);

  variable_set('suggestion_synced', TRUE);

  if (!$last_nid) {
    db_delete('suggestion')->condition('src', SUGGESTION_CONTENT)->execute();
    db_query('UPDATE {suggestion} SET src = src & ' . intval(SUGGESTION_PRIORITY | SUGGESTION_SURFER) . ' WHERE src & ' . SUGGESTION_CONTENT)->execute();
  }
  $stmt = "
    SELECT
      nid,
      title
    FROM
      {node}
    WHERE
      status = 1
      AND nid > :nid
      AND type IN (:types)
    ORDER BY
      nid ASC
  ";
  $args = array(
    ':nid'   => $last_nid,
    ':types' => _suggestion_types(),
  );
  $titles = db_query_range($stmt, 0, $limit, $args)->fetchAllKeyed();

  foreach ($titles as $nid => $title) {
    $count += _suggestion_insert($title, SUGGESTION_CONTENT);
  }
}
/**
 * Drupal AJAX callback form for the "Flush Job Cache" block.
 */
function suggestion_index_block_ajax_callback(&$form, &$form_state) {
  if (!empty($form_state['values']['flush'])) {
    db_truncate('suggestion')->execute();
  }
  suggestion_index();

  return array('#markup' => "<div id=\"suggestion-index-feedback\"><p>Suggestions Indexed</p></div>");
}
/**
 * Generate a Drupal form to switch settings context.
 */
function suggestion_index_block_form() {
  $form['feedback'] = array(
    '#markup' => '<div id="suggestion-index-feedback">' . (variable_get('suggestion_synced', TRUE)? t('No indexing required.'):  t('Indexing required.')) . '</div>',
    '#weight' => 10,
  );
  $form['flush'] = array(
    '#title'         => t('Flush all suggestions'),
    '#description'   => t('Flushes all suggestions including priority and surfer suggestions.'),
    '#type'          => 'checkbox',
    '#default_value' => FALSE,
    '#required'      => FALSE,
    '#weight'        => 20,
  );
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Index Suggestions'),
    '#weight' => 30,
    '#ajax'   => array(
      'callback' => 'suggestion_index_block_ajax_callback',
      'effect'   => 'fade',
      'method'   => 'replace',
      'wrapper'  => 'suggestion-index-feedback',
    ),
  );
  return $form;
}
/**
 * Implements hook_menu().
 */
function suggestion_menu() {
  return array(
    'admin/config/suggestion' => array(
      'title'            => 'Suggestion Config',
      'description'      => 'Custom settings for Suggestion auto-complete.',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('suggestion_settings_form'),
      'access arguments' => array('administer suggestion'),
      'file'             => 'suggestion.admin.inc',
      'type'             => MENU_NORMAL_ITEM,
    ),
    'suggestion/autocomplete' => array(
      'page callback'     => 'suggestion_autocomplete',
      'access callback'   => TRUE,
      'type'              => MENU_CALLBACK,
      'delivery callback' => 'ajax_callback',
    ),
  );
}
/**
 * Implements hook_node_insert().
 */
function suggestion_node_insert($node) {
  if (!empty($node->status)) {
    _suggestion_insert($node->title, SUGGESTION_CONTENT);
  }
}
/**
 * Implements hook_permission().
 */
function suggestion_permission() {
  return array(
    'administer suggestion' => array(
      'title'       => t('Administer the Suggestion Module'),
      'description' => t('Perform administration tasks for the suggestion module.'),
    ),
  );
}
/**
 * Custom submit function to add surfer suggestions.
 */
function suggestion_surfer_submit($form, &$form_state) {
  $field_name = variable_get('suggestion_field_name', '');
  $min_score = 1;

  if (empty($form_state['values'][$field_name])) {
    return;
  }
  $txt = _suggestion_tokenize($form_state['values'][$field_name]);

  if (!$txt) {
    return;
  }
  $atoms = _suggestion_atomize($txt);

  $ngram = implode(' ', $atoms);

  $qty = db_query("SELECT IFNULL(SUM(qty), 0) FROM {suggestion} WHERE ngram = :ngram", array(':ngram' => $ngram))->fetchField();

  if ($qty) {
    db_update('suggestion')->fields(array('qty' => $qty + 1))->condition('ngram', $ngram)->execute();
    return;
  }
  $score = _suggestion_score($atoms);

  if ($score >= $min_score) {
    _suggestion_insert($ngram, SUGGESTION_SURFER);
  }
}
/**
 * Turn the tokenized string to an array with matching key/value pairs.
 *
 * @param string $txt
 *   The title to index.
 *
 * @return array
 *   An array of atoms.
 */
function _suggestion_atomize($txt = '') {
  $stopwords = _suggestion_stopwords();
  $atoms = array();

  foreach (preg_split('/\s+/', $txt) as $atom) {
    if (!empty($stopwords[$atom])) {
      continue;
    }
    $atoms[$atom] = $atom;
  }
  return $atoms;
}
/**
 * Add a priority suggestion to the index.
 *
 * @param string $txt
 *   The title to index.
 * @param int $src
 *   The bits to OR with the current bitmap.
 *
 * @return int
 *   The new bitmap value.
 */
function _suggestion_bitmap($ngram = '', $src = SUGGESTION_CONTENT) {
  $args = array(
    ':src'   => $src,
    ':ngram' => $ngram,
  );
  return db_query("SELECT IFNULL(SUM(src), 0) | :src FROM {suggestion} WHERE ngram = :ngram", $args)->fetchField();
}
/**
 * Add a priority suggestion to the index.
 *
 * @param string $txt
 *   The title to index.
 * @param int $src
 *   The bits to OR with the current bitmap.
 *
 * @return int
 *   The number of suggestions inserted.
 */
function _suggestion_insert($txt = '', $src = SUGGESTION_CONTENT) {
  $count = 0;
  $max = variable_get('suggestion_max', 45);
  $txt = _suggestion_tokenize($txt);

  if (!$txt) {
    return 0;
  }
  $atoms = _suggestion_atomize($txt);

  foreach (array_keys(_suggestion_ngrams($atoms)) as $ngram) {
    if (strlen($ngram) > $max) {
      continue;
    }
    $qty = db_query_range("SELECT IFNULL(SUM(qty), 0) FROM {suggestion} WHERE ngram = :ngram", 0, 1, array(':ngram' => $ngram))->fetchField();

    $key = array('ngram' => $ngram);
    $fields = array(
      'atoms'   => str_word_count($ngram),
      'density' => 1.0,
      'qty'     => $qty + 1,
      'src'     => _suggestion_bitmap($ngram, $src),
    );
    db_merge('suggestion')->key($key)->fields($fields)->execute();

    $count++;
  }
  return $count;
}
/**
 * Build a set of suggestions from the set of atoms.
 *
 * @param array $atoms
 *   An array of strings.
 *
 * @return array
 *   An array of suggestion keys.
 */
function _suggestion_ngrams($atoms = array()) {
  $count = count($atoms);
  $max = variable_get('suggestion_atoms', 6);
  $ngrams = array();

  for ($i = 0; $i < $count; $i++) {
    for ($j = 1; $j <= $max; $j++) {
      $ngrams[implode(' ', array_slice($atoms, $i, $j))] = 1;
    }
  }
  $atoms = array_reverse($atoms);

  for ($i = 0; $i < $count; $i++) {
    for ($j = 1; $j <= $max; $j++) {
      $ngrams[implode(' ', array_slice($atoms, $i, $j))] = 1;
    }
  }
  return $ngrams;
}
/**
 * Build a set of suggestions from the set of atoms.
 *
 * @param array $atoms
 *   An array of strings.
 *
 * @return int
 *   The suggestion's score.
 */
function _suggestion_score($atoms = array()) {
  $query = db_select('field_data_body', 'b');

  $query->fields('b', array('entity_id'));
  $query->join('node', 'n', 'n.nid = b.entity_id');
  $query->condition('n.status', 1);
  $query->condition('n.type', _suggestion_types(), 'IN');

  foreach ($atoms as $atom) {
    $query->condition('b.body_value', '%' . db_like($atom) . '%', 'LIKE');
  }
  return $query->execute()->rowCount();
}
/**
 * Retrieve an array of stopwords.
 *
 * @return array
 *   An array of stopwords.
 */
function _suggestion_stopwords() {
  $stopwords = &drupal_static(__FUNCTION__, NULL);

  if (is_array($stopwords)) {
    return $stopwords;
  }
  $file_path = drupal_realpath(variable_get('suggestion_stopword_uri', ''));

  $stopwords = file_get_contents($file_path);
  $stopwords = array_fill_keys(preg_split('/\s*[\n\r]+\s*/s', $stopwords), 1);

  return $stopwords;
}
/**
 * Tokenize the text into space seperated lowercase strings.
 *
 * @param string $txt
 *   The text to process.
 *
 * @return string
 *   The tokenized string.
 */
function _suggestion_tokenize($txt = '') {
  $min = variable_get('suggestion_min', 4) - 1;

  $regx = array(
    '/[^a-z]+/'                  => ' ',
    '/\b(\w{1,' . $min . '})\b/' => '',
    '/\s\s+/s'                   => ' ',
    '/^\s+|\s+$/s'               => '',
  );
  return preg_replace(array_keys($regx), array_values($regx), strtolower(trim($txt)));
}
/**
 * Build an array of content types used in auto-complete.
 *
 * @return array
 *   An array of enabled content types.
 */
function _suggestion_types() {
  $types = array();

  foreach (variable_get('suggestion_types', array()) as $type => $status) {
    if ($status) {
      $types[] = $type;
    }
  }
  return $types;
}
