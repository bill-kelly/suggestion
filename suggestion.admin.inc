<?php

/**
 * @file
 * Administration form for suggestion module.
 */

/**
 * Menu callback to configure suggestion settings.
 */
function suggestion_settings_form($form) {
  $form['#submit'][] = 'suggestion_settings_form_submit';

  $form['suggestion_form_id'] = array(
    '#title'         => t('Form ID'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('suggestion_form_id', ''),
    '#required'      => TRUE,
    '#weight'        => 10,
  );
  $form['suggestion_field_name'] = array(
    '#title'         => t('Field Name'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('suggestion_field_name', ''),
    '#required'      => TRUE,
    '#weight'        => 20,
  );
  $form['suggestion_min'] = array(
    '#title'         => t('Minimum Characters'),
    '#type'          => 'select',
    '#options'       => array_combine(range(3, 10), range(3, 10)),
    '#default_value' => variable_get('suggestion_min', 4),
    '#required'      => TRUE,
    '#weight'        => 30,
  );
  $form['suggestion_max'] = array(
    '#title'         => t('Maximum Characters in a Suggestion'),
    '#type'          => 'select',
    '#options'       => array_combine(range(20, 60), range(20, 60)),
    '#default_value' => variable_get('suggestion_max', 45),
    '#required'      => TRUE,
    '#weight'        => 40,
  );
  $form['suggestion_atoms'] = array(
    '#title'         => t('Maximum Words in a Suggestion'),
    '#type'          => 'select',
    '#options'       => array_combine(range(1, 10), range(1, 10)),
    '#default_value' => variable_get('suggestion_atoms', 6),
    '#required'      => TRUE,
    '#weight'        => 50,
  );
  $form['suggestion_limit'] = array(
    '#title'         => t('Maximum Suggestions Returned'),
    '#type'          => 'select',
    '#options'       => array_combine(range(10, 100), range(10, 100)),
    '#default_value' => variable_get('suggestion_limit', 20),
    '#required'      => TRUE,
    '#weight'        => 60,
  );
  $form['suggestion_types'] = array(
    '#title'         => t('Content Types'),
    '#type'          => 'checkboxes',
    '#options'       => _suggestion_admin_get_content_types(),
    '#default_value' => variable_get('suggestion_types', array()),
    '#required'      => TRUE,
    '#weight'        => 70,
  );
  $form['suggestion_keywords'] = array(
    '#title'         => t('Priority Suggestions'),
    '#description'   => t('Suggestions entered here take priority.'),
    '#type'          => 'textarea',
    '#default_value' => implode("\n", _suggestion_admin_get_keywords()),
    '#rows'          => 10,
    '#required'      => FALSE,
    '#weight'        => 80,
  );
  $form['suggestion_stopwords'] = array(
    '#title'         => t('Stopwords'),
    '#description'   => t('Stopwords are not indexed.'),
    '#type'          => 'textarea',
    '#default_value' => implode("\n", array_keys(_suggestion_stopwords())),
    '#rows'          => 10,
    '#required'      => FALSE,
    '#weight'        => 90,
  );
  return system_settings_form($form);
}
/**
 * Custom submit function for the suggestion setting form.
 */
function suggestion_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['suggestion_atoms'] != variable_get('suggestion_atoms', 0)) {
    variable_set('suggestion_synced', FALSE);
  }
  elseif ($form_state['values']['suggestion_limit'] != variable_get('suggestion_limit', 0)) {
    variable_set('suggestion_synced', FALSE);
  }
  elseif ($form_state['values']['suggestion_max'] != variable_get('suggestion_max', 0)) {
    variable_set('suggestion_synced', FALSE);
  }
  elseif ($form_state['values']['suggestion_min'] != variable_get('suggestion_min', 0)) {
    variable_set('suggestion_synced', FALSE);
  }
  elseif ($form_state['values']['suggestion_types'] != variable_get('suggestion_types', NULL)) {
    variable_set('suggestion_synced', FALSE);
  }
  else {
    variable_set('suggestion_synced', TRUE);
  }
  _suggestion_admin_submit_keywords($form_state);

  _suggestion_admin_submit_stopwords($form_state);
}
/**
 * Retrieve a set of content type options.
 *
 * @return array
 *   An array of content type options.
 */
function _suggestion_admin_get_content_types() {
  return db_query("SELECT type, name FROM {node_type} ORDER BY name ASC, type ASC")->fetchAllKeyed();
}
/**
 * Retrieve an array of stopwords.
 *
 * @return array
 *   An array of stopwords.
 */
function _suggestion_admin_get_keywords() {
  return db_query("SELECT ngram FROM {suggestion} WHERE src & :src ORDER BY ngram ASC", array(':src' => SUGGESTION_PRIORITY))->fetchCol();
}
/**
 * Add a priority suggestion to the index.
 *
 * @param string $txt
 *   The title to index.
 */
function _suggestion_admin_priority_insert($txt = '') {
  $txt = _suggestion_tokenize($txt);

  if (!$txt) {
    return;
  }
  $atoms = _suggestion_atomize($txt);

  foreach (array_keys(_suggestion_ngrams($atoms)) as $ngram) {
    $key = array('ngram' => $ngram);
    $fields = array(
      'atoms'   => str_word_count($ngram),
      'density' => 1.0,
      'qty'     => db_query("SELECT IFNULL(MAX(qty), 1) FROM {suggestion}")->fetchField(),
      'src'     => _suggestion_bitmap($ngram, SUGGESTION_PRIORITY),
    );
    db_merge('suggestion')->key($key)->fields($fields)->execute();
  }
}
/**
 * Process keywords.
 */
function _suggestion_admin_submit_keywords(&$form_state) {
  foreach (preg_split('/\s*[\n\r]+\s*/s', $form_state['input']['suggestion_keywords']) as $txt) {
    _suggestion_admin_priority_insert($txt);
  }
  unset($form_state['input']['suggestion_keywords']);
  unset($form_state['values']['suggestion_keywords']);
}
/**
 * Process stopwords.
 */
function _suggestion_admin_submit_stopwords(&$form_state) {
  $stopwords = array();

  foreach (preg_split('/\s*[\n\r]+\s*/s', $form_state['input']['suggestion_stopwords']) as $txt) {
    $stopwords += array_flip(preg_split('/\s+/', _suggestion_tokenize($txt)));
  }
  $stopwords = array_fill_keys(array_keys($stopwords), 1);

  ksort($stopwords);

  $file_path = drupal_realpath(variable_get('suggestion_stopword_uri', 'public://suggestion/suggestion_stopword.txt'));

  $stopwords = implode("\n", array_keys($stopwords));

  $hash = drupal_hash_base64($stopwords);

  if ($hash != variable_get('suggestion_stopword_hash', '')) {
    variable_set('suggestion_stopword_hash', $hash);
    variable_set('suggestion_synced', FALSE);
  }
  file_unmanaged_save_data($stopwords, $file_path, FILE_EXISTS_REPLACE);

  unset($form_state['input']['suggestion_stopwords']);
  unset($form_state['values']['suggestion_stopwords']);
}
